#ifndef PACKET_H
#define PACKET_H
#include <string>

using namespace std ;

/// \~English
/// \brief A class to represent packets.
///
/// The class contains two constructurs and various functions
/// for searching and filtering network flow packets.
/// \~Spanish
/// \brief Una clase que representa paquetes.
///
/// La clase contiene dos constructor y varias funciones
/// para buscar y filtrar paquetes de fluidos de red.
class Packet{
    public:
    /// \fn Packet::Packet()
    /// \~English
    /// \brief Default constructor. The properties of the packet are set as follows:
    /// * src_addr: set to the empty string.
    /// * dst_addr: set to the empty string.
    /// * src_port: set to 0.
    /// * dst_port: set to 0.
    /// * octects: set to 0.
    /// * packets: set to 0.
    /// * enabled: set to false.
    /// \~Spanish
    /// \brief Constructor por defecto.  Las propiedades de el paquete se ajustan como sigue:
    /// * src_addr: a una cadena vacia.
    /// * dst_addr: a una cadena vacia.
    /// * src_port: a 0.
    /// * dst_port: a 0.
    /// * octects: a 0.
    /// * packets: a 0.
    /// * enabled: a falso.
        Packet();

        /// \fn Packet::Packet(string sa, string da, int sp, int dp, int oct, int pkt)
        /// \~English
        /// \brief Constructor which accepts specification for sa(src_addr),
        /// da(dst_addr), sp(src_port), dp(dst_port), oct(octects) and
        /// pkt(packets)
        /// \param sa source address
        /// \param da destination address
        /// \param sp source port
        /// \param dp destination port
        /// \param oct octects
        /// \param pkt packets
        /// \~Spanish
        /// \brief Constructor que acepta especificaciones para sa(src_addr),
        /// da(dst_addr), sp(src_port), dp(dst_port), oct(octects) y
        /// pkt(packets)
        /// \param sa direccion fuente
        /// \param da direccion destino
        /// \param sp puerto fuente
        /// \param dp puerto destino
        /// \param oct octetos
        /// \param pkt paquetes
        Packet(string, string, int, int, int, int) ;

        /// \fn string Packet::getSrcAddr()
        /// \~English
        /// \brief Getter for the source addr
        /// \return src address
        /// \~Spanish
        /// \brief Devuelve la direccion fuente
        /// \return direccion fuente
        string getSrcAddr() ;

        /// \fn string Packet::getDstAddr()
        /// \~English
        /// \brief Getter for the destination addr
        /// \return dst address
        /// \~Spanish
        /// \brief Devuelve la direccion destino
        /// \return direccion destino
        string getDstAddr() ;

        /// \fn string Packet::getSrcPort()
        /// \~English
        /// \brief Getter for the source port
        /// \return src port
        /// \~Spanish
        /// \brief Devuelve el puerto fuente
        /// \return puerto fuente
        int getSrcPort() ;

        /// \fn string Packet::getDstPort()
        /// \~English
        /// \brief Getter for the destination port
        /// \return dst port
        /// \~Spanish
        /// \brief Devuelve la puerto destino
        /// \return puerto destino
        int getDstPort() ;

        /// \fn string Packet::getOctets()
        /// \~English
        /// \brief Getter for the octets
        /// \return octets
        /// \~Spanish
        /// \brief Devuelve los octetos
        /// \return octetos
        int getOctects() ;

        /// \fn string Packet::getPackets()
        /// \~English
        /// \brief Getter for the packets
        /// \return packets
        /// \~Spanish
        /// \brief Devuelve los paquetes
        /// \return paquetes
        int getPackets() ;

        /// \fn void Packet::setSrcAddr(string addr){
        /// \~English
        /// \brief Setter for the src_addr
        /// \param addr source address
        /// \~Spanish
        /// \brief Ajusta la direccion fuente
        /// \param addr direccion fuente
        void setSrcAddr(string) ;

        /// \fn void Packet::setDstAddr(string addr){
        /// \~English
        /// \brief Setter for the destination addr
        /// \param addr destination address
        /// \~Spanish
        /// \brief Ajusta la direccion destino
        /// \param addr direccion destino
        void setDstAddr(string) ;

        /// \fn void Packet::setSrcPort(int port){
        /// \~English
        /// \brief Setter for the source port
        /// \param port source port
        /// \~Spanish
        /// \brief Ajusta el puerto fuente
        /// \param port puerto fuente
        void setSrcPort(int) ;

        /// \fn void Packet::setDstPort(int port){
        /// \~English
        /// \brief Setter for the destination port
        /// \param port destination port
        /// \~Spanish
        /// \brief Ajusta el puerto destino
        /// \param port puerto destino
        void setDstPort(int) ;

        /// \fn void Packet::setOctetes(int val){
        /// \~English
        /// \brief Setter for the octects value
        /// \param val octects
        /// \~Spanish
        /// \brief Ajusta el valor de los octetos
        /// \param val octetos
        void setOctects(int) ;

        /// \fn void Packet::setPackets(int val){
        /// \~English
        /// \brief Setter for the packets value
        /// \param val packets
        /// \~Spanish
        /// \brief Ajusta el valor de los paquetes
        /// \param val paquetes
        void setPackets(int) ;

        /// \fn void Packet::enable(){
        /// \~English
        /// \brief Sets that the packet is enabled to be displayed in GUI
        /// \~Spanish
        /// \brief Ajusta que el paquete esta disponible para ser desplegado en la inter
        /// fase de usuario
        void enable() ;

        /// \fn void Packet::disable(){
        /// \~English
        /// \brief Sets that the packet is disabled to be displayed in GUI
        /// \~Spanish
        /// \brief Ajusta que el paquete no esta disponible para ser desplegado en la inter
        /// fase de usuario
        void disable() ;

        /// \fn void Packet::isEnable(){
        /// \~English
        /// \brief Returns true if the packet is enabled, false otherwise
        /// \return true (enable) or false
        /// \~Spanish
        /// \brief Devuelve cierto si el paquete esta disponible, falso de lo contrario
        bool isEnabled() ;

    private:
        string src_addr ; /**< src_addr source address (direccion fuente)*/
        string dst_addr ; /**< dst_addr destination address (direccion destino)*/
        int src_port ; /**< src_port source port (puerto fuente)*/
        int dst_port ; /**< dst_port destination port (puerto destino)*/
        int octects ; /**< octets/bytes value (octetos)*/
        int packets ; /**< packets value (paquetes)*/
        bool enabled ;/**< enabled in the GUI (disponible en el GUI)*/
};

#endif // PACKET_H
